﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Algorithms.Sorting;

namespace BubbleSortTests
{
    [TestClass]
    public class SortTests
    {
        [TestMethod]
        public void NormalSortTest()
        {
            List<int> actualList = new List<int>() { 5, 2, 1, 8, 10, 3, 7 };
            List<int> expectedList = new List<int>() { 10, 8, 7, 5, 3, 2, 1 };

            BubbleSort.Sort(actualList);

            CollectionAssert.AreEqual(expectedList, actualList);
        }

        [TestMethod]
        public void RecursiveSortTest()
        {
            List<int> actualList = new List<int>() { 5, 2, 1, 8, 10, 3, 7 };
            List<int> expectedList = new List<int>() { 10, 8, 7, 5, 3, 2, 1 };

            BubbleSort.SortRecursive(actualList);

            CollectionAssert.AreEqual(expectedList, actualList, "test msg");
        }
    }
}
