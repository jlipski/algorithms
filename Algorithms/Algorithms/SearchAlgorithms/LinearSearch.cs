﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Algorithms.SearchAlgorithms
{
    class LinearSearch
    {
        public static int Find(List<int> list, int x, int n)
        {
                for (int i = n; i < list.Count; i++)
                {
                    if (list[i] == x)
                    {
                        return i;
                    }
                }

            return -1;
        }

        public static void FindAllElements(List<int> list, int x)
        {
            int result;
            int tmp = 0;

            for (int i = 0; i < list.Count; i++)
            {
                result = Find(list, x, i);

                if(result != -1)
                {
                    Debug.WriteLine("Match found on position: " + result);
                    i = result;
                    tmp++;

                }

                
            }
            if (tmp == 0)
            {
                Debug.WriteLine("No matches found");
            }
        }
    }
}
