﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Algorithms.Sorting
{
    public class BubbleSort
    {
        public static List<int> Sort(List<int> list)
        {
            int n = 0;

            while (n < list.Count){
                
                for (int i = 1; i < list.Count - n; i++)
                {
                    if (list[i] > list[i - 1])
                    {
                        int tmp = list[i];
                        list[i] = list[i - 1];
                        list[i - 1] = tmp;
                    }
                }

                n++;
            } 

            return list;
        }

        private static List<int> SortRecursive(List<int> list, int n)
        {
            if (n < list.Count)
            {
                for (int i = 1; i < list.Count - n; i++)
                {
                    if (list[i] > list[i - 1])
                    {
                        int tmp = list[i];
                        list[i] = list[i - 1];
                        list[i - 1] = tmp;
                    }
                }

                n++;
                return SortRecursive(list, n);
            }
            else
            {
                return list;
            }
        }

        public static List<int> SortRecursive(List<int> list)
        {
            return SortRecursive(list, 0);
        }
    }
}
